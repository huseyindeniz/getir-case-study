# getir-case-study

## Quick Start

Get started developing...

You need to create a `.env` file and configure your database at first, then set up the database and start developing / testing.

```shell
# install deps
yarn install

# run in development mode
yarn dev

# run tests
yarn test

# check code styles
yarn lint

# check type errors
yarn typecheck
```

---

## Install Dependencies

Install all package dependencies (one time operation)

```shell
yarn install
```

## Run It

#### Run in _development_ mode:

Runs the application is development mode. Should not be used in production

```shell
yarn dev
```

or debug it

```shell
yarn dev:debug
```

#### Run in _production_ mode:

Compiles the application and starts it in production mode

```shell
yarn compile
yarn start
```

## Test It

Run the Mocha unit tests

```shell
yarn test
```

Run the tests and output a JUnit-style XML test results file at `./test/test-results.xml`

```shell
yarn test:junit
```

## Try It

Make sure the database is running

- Open you're browser to [http://localhost:3000](http://localhost:3000)
- Invoke the `/records` endpoint
  ```shell
  curl http://localhost:3000/api/records
  ```


---
### POST api/records


**Request** 

```
{
  "startDate": "2016-01-26",
  "endDate": "2018-02-02",
  "minCount": 100,
  "maxCount": 130
}
```
**Response** 

```
{
    "msg": "Success",
    "code": 0,
    "records": [
        {
            "key": "TAKwGc6Jr4i8Z487",
            "createdAt": "2017-01-28T01:22:14.398Z",
            "totalCount": 120
        }
    ]
}
```