export class Utils {
  
  // Checks whether the request parameters are correct.
  isValidRequest(req) {
    let errors = {};
    if (!this.isValidDate(req.startDate) || req.startDate === null || typeof req.startDate === undefined || req.startDate === "" || req.startDate.length === 0) {
      errors["startDate"] = "startDate is incorrect. Date format should be like 'YYYY-MM-DD'"
    }
    if (!this.isValidDate(req.endDate) || req.endDate === null || typeof req.endDate === undefined || req.endDate === "" || req.endDate.length === 0) {
      errors["endDate"] = "endDate is incorrect. Date format should be like 'YYYY-MM-DD'"
    }
    if (req.minCount === null || typeof req.minCount === undefined || req.minCount === "" || req.minCount.length === 0 || req.minCount < 0) {
      errors["minCount"] = "minCount must be greater than 0."
    }
    if (req.maxCount === null || typeof req.maxCount === undefined || req.maxCount === "" || req.maxCount.length === 0 || req.maxCount < 0) {
      errors["maxCount"] = "maxCount must be greater than 0."
    }
    return errors;
  }
  
  isEmptyObject(obj) {
    for (let key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        return false;
      }
    }
    return true;
  }
  
  // Checks whether the date format is correct.
  isValidDate(dateString) {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    if(!dateString.match(regEx)) return false;  
    let d = new Date(dateString);
    let dNum = d.getTime();
    if(!dNum && dNum !== 0) return false;
    return d.toISOString().slice(0,10) === dateString;
  }
}

export default new Utils();