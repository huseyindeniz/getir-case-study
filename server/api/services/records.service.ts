import mongoose from "mongoose";

export class RecordsService {
  async getRecords(filters): Promise<any> {
    let records: any[];

    const db = mongoose.connection;
    let collection = db.collection("records");

    let options = {
      allowDiskUse: false
    };
    
    let pipeline = [
      {
        "$project": {
          "_id": 0.0,
          "key": 1.0,
          "createdAt": 1.0,
          "totalCount": {
            "$sum": "$counts"
          }
        }
      }, 
      {
        "$match": {
          "createdAt": {
            "$gte": new Date(filters.startDate),
            "$lte": new Date(filters.endDate),
          },
          "totalCount": {
            "$gt": filters.minCount,
            "$lt": filters.maxCount
          }
        }
      }
    ];
  
    let cursor = collection.aggregate(pipeline, options);
  
    records = await cursor.toArray();
    return records;
  }
}

export default new RecordsService();
