import RecordsService from "../../services/records.service";
import { Request, Response, NextFunction } from "express";
import utils from "../../../common/utils";

export class Controller {
  async getRecords(req: Request, res: Response, next: NextFunction) {
    try {
      const isValid = utils.isValidRequest(req.body);
      if(!utils.isEmptyObject(isValid)) {
        return res.status(400).json({
          code: -1,
          msg: isValid,
          records: []
        });
      } else {
        const docs = await RecordsService.getRecords(req.body);
        return res.status(200).json({
          code: 0,
          msg: "Success",
          records: docs
        });
      }
    } catch (err) {
      return next(err);
    }
  }
}

export default new Controller();
