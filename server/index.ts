import "./common/env";
import Database from "./common/database";
import Server from "./common/server";
import routes from "./routes";

const port = parseInt(process.env.PORT || "3000");
const connectionString =
  process.env.NODE_ENV === "production"
    ? process.env.MONGODB_URI
    : process.env.NODE_ENV === "test"
    ? process.env.MONGODB_URI_TEST ||
      "mongodb+srv://admin:admin@cluster0.xad6z.mongodb.net/getir-case-study?authSource=admin&replicaSet=atlas-a7h4kt-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true"
    : process.env.MONGODB_URI_DEV ||
      "mongodb+srv://admin:admin@cluster0.xad6z.mongodb.net/getir-case-study?authSource=admin&replicaSet=atlas-a7h4kt-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true";

export const db = new Database(connectionString);
export default new Server().database(db).router(routes).listen(port);
