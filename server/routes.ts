import { Application } from "express";
import recordsRouter from "./api/controllers/records/router";
export default function routes(app: Application): void {
  app.use("/api/records", recordsRouter);
}
