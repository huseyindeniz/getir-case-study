import "mocha";
import { expect } from "chai";
import request from "supertest";
import Server from "../server";

describe("Records", () => {

  it("should get records", () =>
    request(Server)
      .post("/api/records")
      .send({
        "startDate": "2016-01-26",
        "endDate": "2018-02-02",
        "minCount": 100,
        "maxCount": 130
      })
      .expect(200)
      .expect("Content-Type", /json/)
      .then((r) => {
        expect(r.body)
          .to.be.an.instanceof(Object)
          .and.to.have.keys(['code', 'msg', 'records'])
      }));
});
