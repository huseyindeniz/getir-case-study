import "mocha";
import mongoose from "mongoose";

before(function (done) {
  this.timeout(15000);
  const db = mongoose.connection;
  db.once("open", async () => {
    setTimeout(done, 3000);
  });
});
